import {useState, useEffect} from 'react';

import AppNavbar from './components/AppNavBar';
import ProductView from './components/ProductView';

import Home from './pages/Home';
import Product from './pages/Product';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminPanel from './pages/AdminPanel'

import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css'; 

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {

          // User is logged in
          if(typeof data._id !== "undefined") {

              setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
              })
          } 
          else { 
              setUser({
                  id: null,
                  isAdmin: null
              })
          }

      })
  }, []);
  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          < AppNavbar/>
          <Container>
          <Routes>
            < Route path="/" element={<Home/>}/>
            < Route path="/products" element={<Product/>}/>
            < Route path="/products/:productId" element={<ProductView/>}/>
            < Route path="/register" element={<Register/>}/>
            < Route path="/login" element={<Login />}/>
            < Route path="/logout" element={<Logout/>}/>
            < Route path="/adminPanel" element={<AdminPanel/>}/>
            < Route path="/*" element={<Error />} />
          </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;

