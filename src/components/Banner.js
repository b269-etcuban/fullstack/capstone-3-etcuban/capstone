import {Button, Row, Col} from 'react-bootstrap';

export default function Banner() {
  return (
    <Row>
      <Col className="p-5">
        <h1>BREMOD</h1>
        <p>Get Beautiful Shine Hair</p>
        <Button variant="primary">Buy Now!</Button>
      </Col>
    </Row>

  )
}