import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		destination: "/home",
	}


	return (
		<>
		<Banner data={data} />
    	<Highlights />
    	
		</>
	)
}