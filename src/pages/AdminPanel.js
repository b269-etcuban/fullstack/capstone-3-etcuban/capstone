import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AdminPanel() {
  const [products, setProducts] = useState([]);
  const [newProduct, setNewProduct] = useState({ name: '', description: '', price: '', active: true });
  const [updateProduct, setUpdateProduct] = useState({ id: null, name: '', description: '', price: '', active: true });
  const {user} = useContext(UserContext);

  // Redirect to login page if user is not authenticated
  useEffect(() => {
    if (!user) {
      return <Navigate to="/login" />;
    }
  }, [user]);

  // Retrieve all products from the backend API
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then(res => res.json())
      .then(data => setProducts(data));
  }, []);

  // Create a new product and add it to the list of products
  const handleNewProductSubmit = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newProduct)
    })
      .then(res => res.json())
      .then(data => {
        setProducts([...products, data]);
        setNewProduct({ name: '', description: '', price: '', active: true });
      });
  };

  // Update a product and update the list of products
  const handleUpdateProductSubmit = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/products/${updateProduct.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(updateProduct)
    })
      .then(res => res.json())
      .then(data => {
        const updatedProducts = products.map(p => {
          if (p.id === data.id) {
            return data;
          } else {
            return p;
          }
        });
        setProducts(updatedProducts);
        setUpdateProduct({ id: null, name: '', description: '', price: '', active: true });
      });
  };

  // Deactivate or reactivate a product and update
  const handleToggleProduct = (product) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${product.id}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({...product, active: !product.active})
    })
      .then(res => res.json())
      .then(data => {
        const updatedProducts = products.map(p => {
          if (p.id === data.id) {
            return data;
          } else {
            return p;
          }
        });
        setProducts(updatedProducts);
      });
  };

  // Delete a product and update the list of products
  const handleDeleteProduct = (product) => {
    Swal.fire({
      title: 'Are you sure?',
      text: `You are about to delete ${product.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/products/deleteProduct/${product.id}`, {
            method: 'DELETE'
        })
          .then(res => res.json())
          .then(data => {
            const updatedProducts = products.filter(p => p.id !== product.id);
            setProducts(updatedProducts);
          })
          .catch(err => console.log(err));
        Swal.fire(
            'Deleted!',
            `${product.name} has been deleted.`,
            'success'
        );
      }
    })
  }
  
  return (
    <div>
      <h1 className="text-center">Admin DashBoard</h1>
      {/* form for creating new products */}
      <Form onSubmit={handleNewProductSubmit}>
        {/* form fields for new product */}
        <Form.Group controlId="newProductName">
          <Form.Label>Name</Form.Label>
          <Form.Control type="text" placeholder="Enter name" value={newProduct.name} onChange={(e) => setNewProduct({ ...newProduct, name: e.target.value })} required />
        </Form.Group>

        <Form.Group controlId="newProductDescription">
          <Form.Label>Description</Form.Label>
          <Form.Control type="text" placeholder="Enter description" value={newProduct.description} onChange={(e) => setNewProduct({ ...newProduct, description: e.target.value })} required />
        </Form.Group>

        <Form.Group controlId="newProductPrice">
          <Form.Label>Price</Form.Label>
          <Form.Control type="number" placeholder="Enter price" min="0" step="0.01" value={newProduct.price} onChange={(e) => setNewProduct({ ...newProduct, price: e.target.value })} required />
        </Form.Group>

        <Button variant="primary" type="submit">Create</Button>
      </Form>

      <hr />

      <h2 className="text-center">Products</h2>
      {/* display list of products */}
      {products.map((product) => (
        <div key={product.id}>
          <p>{product.name}</p>
          <p>{product.price}</p>
          <p>{product.active ? "Active" : "Inactive"}</p>

          {/* buttons for editing and deactivating/reactivating product */}
          <Button variant="primary" onClick={() => setUpdateProduct(product)}>Edit</Button>{' '}
          <Button variant={product.active ? "warning" : "success"} onClick={() => handleToggleProduct(product)}>
            {product.active ? "Deactivate" : "Reactivate"}
          </Button>{' '}
          <Button variant="danger" onClick={() => handleDeleteProduct(product)}>Delete</Button>{' '}
        </div>
      ))}

      {/* form for updating existing products */}
      {updateProduct.id && (
        <Form onSubmit={handleUpdateProductSubmit}>
          {/* form fields for updating product */}
          <Form.Group controlId="updateProductName">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" placeholder="Enter name" value={updateProduct.name} onChange={(e) => setUpdateProduct({ ...updateProduct, name: e.target.value })} required />
          </Form.Group>

          <Form.Group controlId="updateProductDescription">
            <Form.Label>Description</Form.Label>
            <Form.Control type="text" placeholder="Enter description" value={updateProduct.description} onChange={(e) => setUpdateProduct({ ...updateProduct, description: e.target.value })} required />
          </Form.Group>

          <Form.Group controlId="updateProductPrice">
            <Form.Label>Price</Form.Label>
            <Form.Control type="number" placeholder="Enter price" min="0" step="0.01" value={updateProduct.price} onChange={(e) => setUpdateProduct({ ...updateProduct, price: e.target.value })} required />
          </Form.Group>

          <Button variant="primary" type="submit">Update</Button>{' '}
          <Button variant="secondary" onClick={() => setUpdateProduct({ id: null, name: '', description: '', price: '', active: true })}>Cancel</Button>
        </Form>
      )}
    </div>
  )
}