import {useState, useEffect} from 'react';

import ProductCard from '../components/ProductCard';
import AdminPanel from '../pages/AdminPanel'

export default function Product() {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} product={product} />
				)
			}))
		})
	}, [])

	const isAdmin = true;

	return (
		<>
		    {isAdmin && <AdminPanel />}
      		{!isAdmin && <ProductCard/>}
		</>
	)
};